const request = require('request-promise');
const FB_URI = `http://localhost:5001`;

const startFacebook = (email) => {
  return new Promise( async (resolve, reject) => {
    const options = {
      uri: `${FB_URI}/start-facebook`,
      method: 'POST',
      body: {
        email
      },
      json: true
    }
    try {
      const facebookStatus = await request(options);
      console.log(`[MS-FB] Start Facebook Result : ${facebookStatus}`);
      resolve();
    } catch (e) {
      console.error("[MS-FB-ERROR] startFacebook : ", e);
      reject("Error in Start Facebook");
    }
  })
}

module.exports = {
  startFacebook
}