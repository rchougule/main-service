const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const storage = require('./storage/index');
const services = require('./services/index');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// 1. start extraction
// 2. check if data exists
// 3. send data back to communication-handler
// 4. start further services

app.post('/start-extraction', async (req, res) => {
  /**
   * 1. check in redis the status of this email id
   * 2. check in db if status doesnt exist in redis
   * 3. publish that the extraction is complete and update in redis. be it partial or complete
   * 4. or begin the extraction process
   */
  const email = req.body.email;
  try {
    let redisStatus = await storage.redisOps.getStatus(email);
    if (redisStatus) {
      res.status(200).json(redisStatus);
    } else {
      let dbStatus = await storage.dbOps.getStatus(email);
      if (dbStatus) {
        storage.redisOps.setStatus(email, dbStatus);
        res.status(200).json(dbStatus);
      } else {
        // set in redis and db about the initial status
        let initiatStatus = {facebook: "Not Started", linkedIn: "Not Started", twitter: "Not Started", quora: "Not Started", glassdoor: "Not Started"};
        storage.redisOps.setStatus(email, initiatStatus);
        storage.dbOps.setStatus(email, initiatStatus);
        // start the facebook service here
        try {
          services.fbServices.startFacebook(email);
        } catch (err) {
          
        }
        res.status(200).json(initiatStatus);
      }
    }
  } catch (err) {
    res.status(500).json({error: err.toString()});
  }
});

app.post('/get-data', (req, res) => {
  /**
   * 1. retrieve data of the given email id
   * 2. revert
   */
  res.status(200).json({data: "call hua bhai"});
});


app.listen(8082, "localhost", () => console.log("Main Service Started on Port 8082"));