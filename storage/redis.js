const redis = require('redis');
const bluebird = require('bluebird');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient();

client.on("error", (err) => {
  console.error(`[MS-REDIS-ERROR] : ${err}`);
})

const getStatus = async (email) => {
  try {
    const status = await client.hgetallAsync(email);
    return(status);
  } catch (err) {
    console.error(`[MS-REDIS-ERROR] getStatus : ${err}`);
  }
}

const setStatus = async (email, status) => {
  client.hmset(email, status);
}

module.exports = {
  getStatus,
  setStatus
}
