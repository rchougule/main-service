const MongoClient = require('mongodb').MongoClient;
const DBNAME = 'syndicate';
const URL = 'mongodb://localhost:27017';

const getStatus = (email) => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(URL, { useNewUrlParser: true}, async (err, client) => {
      if (err) {
        reject("Error in Connection");
      } else {
        const db = client.db(DBNAME);
        const statusCollection = db.collection("status");
        try {
          const status = await statusCollection.findOne({email}, {fields: {_id: 0}});
          if (status) {
            resolve(status);
          } else {
            resolve(false);
          }
        } catch (err) {
          console.error(`[MS-MDB-ERROR] getStatus : ${err}`);
          reject("Error in Get Status");
        }
      }
    })
  })
}

const setStatus = (email, status) => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(URL, { useNewUrlParser: true}, async (err, client) => {
      if (err) {
        reject("Error in Connection");
      } else {
        const db = client.db(DBNAME);
        const statusCollection = db.collection("status");
        try {
          await statusCollection.updateOne({email}, {$set: {...status}}, {upsert: true});
          resolve();
        } catch (err) {
          console.error(`[MS-MDB-ERROR] getStatus : ${err}`);
          reject("Error in Set Status");
        }
      }
    })
  }) 
}

module.exports = {
  getStatus,
  setStatus
}