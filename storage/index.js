const redisOps = require('./redis');
const dbOps = require('./db');

module.exports = {
  redisOps: {
    ...redisOps
  },
  dbOps: {
    ...dbOps
  }
}